<?php
class Db_model extends CI_Model {
    private $_salt = "OnRajouteDuSelPourAllongerleMDP123!!45678__Test";
    function __construct(){
      $this->load->database();
     	parent::__construct();
    }

    function getAllNews() { 
		  $requete="SELECT ACT_id, ACT_Titre, ACT_Contenu, ACT_Date, CPT_Pseudo FROM t_actualite_act
                JOIN t_compte_cpt USING (CPT_id);";
      $query = $this->db->query($requete);
      return $query->result();
    }

    function loginTo($username) {
      $this->db->where('CPT_Pseudo', $username);
      $requete = "SELECT * FROM t_compte_cpt
                  WHERE CPT_Pseudo = '".$pseudo."';";
      $query = $this->db->query($requete);
      return $query->row();
    }
    
    public function connect_compte($username, $password) {
      $passwordHash = hash('sha256', $this->_salt.$password);
      $this->db->where('CPT_Pseudo', $username);
      $this->db->where('CPT_MotDePasse', $passwordHash);
      $this->db->where('CPT_Validite', 'A');
      $query = $this->db->get('t_compte_cpt');
      if($query->num_rows() > 0) 
        {return true;}
      else{return false;}
    }

    public function check_pseudo($pseudo) {
      $this->db->where('CPT_Pseudo', $pseudo);
      $query = $this->db->get('t_compte_cpt');
      if($query->num_rows() > 0) 
        {return true;}
      else{return false;}
    }

    public function add_compte($statut, $pseudo, $prenom, $nom, $mail, $ptretrait, $mdp) {
      $mdpHash = hash('sha256', $this->_salt.$mdp);
      $requeteCPT;
      if($statut == "gestionnaire") {
        $requeteCPT = "INSERT INTO t_compte_cpt VALUES (NULL, '".$pseudo."', '".$mdpHash."', 'G', 'D', NULL);";
      }
      else {
        $ptid = "SELECT PT_id FROM t_point_retrait_pt WHERE PT_Adresse = '".$ptretrait."';";
        $query = $this->db->query($ptid);
        $row = $query->row();
        if(isset($row)) {
          $requeteCPT = "INSERT INTO t_compte_cpt VALUES (NULL, '".$pseudo."', '".$mdpHash."', 'V', 'D', ".$row->PT_id.");";
        }
      }
      $query = $this->db->query($requeteCPT);
      if(isset($query)) {
        $this->db->select_max('CPT_id', 'idMax');
        $requeteidMax = $this->db->get('t_compte_cpt'); // SELECT MAX(CPT_id) AS idMax FROM t_compte_cpt
        $row = $requeteidMax->row();
        if(isset($row)) {
          $requetePFL = "INSERT INTO t_profil_pfl VALUES (NULL, '".$nom."', '".$prenom."', '".$mail."', ".$row->idMax.");";
          $query = $this->db->query($requetePFL);
        }
      }
    }

    public function get_statut($username) {
      $this->db->where('cpt_pseudo', $username);
      $requete = "SELECT CPT_Statut FROM t_compte_cpt
                  WHERE CPT_Pseudo ='".$username."';";
      $query = $this->db->query($requete);
      $statut = $query->row();
      if(isset($statut)) {
        return $statut->CPT_Statut;
      }
    }
    
    public function get_profil() {
      $requete = "SELECT * FROM t_compte_cpt
                  JOIN t_profil_pfl USING (CPT_id)
                  LEFT OUTER JOIN t_point_retrait_pt USING (PT_id)
                  WHERE CPT_Pseudo = '".$_SESSION['login']."';";
      $query = $this->db->query($requete);
      return $query->row();
    }

    public function get_all_profils() {
      $requete = "SELECT * FROM t_compte_cpt
                  JOIN t_profil_pfl USING (CPT_id)
                  LEFT OUTER JOIN t_point_retrait_pt USING (PT_id);";
      $query = $this->db->query($requete);
      return $query->result();
    }

    public function check_profil($prenom, $nom, $mail, $mdp) {
      if($mdp == "") {
        $requete = "SELECT * FROM t_profil_pfl
                    JOIN t_compte_cpt USING(CPT_id)
                    LEFT OUTER JOIN t_point_retrait_pt USING(PT_id)
                    WHERE PFL_Prenom = '".$prenom."'
                    AND PFL_Nom = '".$nom."'
                    AND PFL_Mail = '".$mail."'
                    AND CPT_Validite = 'A'
                    AND CPT_Pseudo = '".$_SESSION['login']."';";
        $query = $this->db->query($requete);
        if($query->num_rows() > 0) 
          {return true;}
        else{return false;}
      }
      else {
        $mdpHash = hash('sha256', $this->_salt.$mdp);
        $requete = "SELECT * FROM t_profil_pfl
                    JOIN t_compte_cpt USING(CPT_id)
                    LEFT OUTER JOIN t_point_retrait_pt USING(PT_id)
                    WHERE PFL_Prenom = '".$prenom."'
                    AND PFL_Nom = '".$nom."'
                    AND PFL_Mail = '".$mail."'
                    AND CPT_Validite = 'A'
                    AND CPT_MotDePasse = '".$mdpHash."'
                    AND CPT_Pseudo = '".$_SESSION['login']."';";
        $query = $this->db->query($requete);
        if($query->num_rows() > 0) 
          {return true;}
        else{return false;}
      }
    }

    public function update_profil($prenom, $nom, $mail) {
      $requete = "UPDATE t_profil_pfl
                  SET PFL_Prenom = '".$prenom."', PFL_Nom = '".$nom."', PFL_Mail = '".$mail."'
                  WHERE CPT_id = (SELECT CPT_id FROM t_compte_cpt
                                  WHERE CPT_Pseudo = '".$_SESSION['login']."');";
      $query = $this->db->query($requete);
    }

    function update_password($mdp) {
      $mdpHash = hash('sha256', $this->_salt.$mdp);
      $requete = "UPDATE t_compte_cpt
                  SET CPT_MotDePasse = '".$mdpHash."'
                  WHERE CPT_Pseudo = '".$_SESSION['login']."';";
      $query = $this->db->query($requete);
    }

    public function check_mdp($mdp1, $mdp2) {
      if($mdp1 == "" && $mdp2 == "") return true;
      if($mdp1 == $mdp2) return true;
      return false;
    }

    public function check_commande($codeClient, $codeCommande) {
      $this->db->where('COM_CodeClient', $codeClient);
      $this->db->where('COM_CodeCommande', $codeCommande);
      $query = $this->db->get('t_commande_com');
      if($query->num_rows() > 0) 
        {return true;}
      else{return false;}
    }

    public function get_all_ptretrait() {
      $query = $this->db->get('t_point_retrait_pt');
      return $query->result();
    }

    public function get_commande($codeClient, $codeCommande) {
      $requete = "SELECT * FROM t_commande_com
                  WHERE COM_CodeClient = '".$codeClient."'
                  AND COM_CodeCommande = '".$codeCommande."';";
      $query = $this->db->query($requete);
      return $query->row();
    }

    public function get_goodies_from_commande($codeClient, $codeCommande) {
      $requete = "SELECT * FROM t_commande_com
                  JOIN GOODIE_has_COMMANDE USING(COM_id)
                  JOIN t_goodie_goo USING(GOO_id)
                  WHERE COM_CodeClient = '".$codeClient."'
                  AND COM_CodeCommande = '".$codeCommande."';";
      $query = $this->db->query($requete);
      return $query->result();
    }

    public function get_prix_total($codeClient, $codeCommande) {
      $requete = "SELECT (COM_Prix + SUM(GOOCOM_QuantiteCommande * GOO_PrixImage)) AS prixMax FROM t_commande_com
                  JOIN GOODIE_has_COMMANDE USING(COM_id)
                  JOIN t_goodie_goo USING(GOO_id)
                  WHERE COM_CodeClient = '".$codeClient."'
                  AND COM_CodeCommande = '".$codeCommande."';";
      $query = $this->db->query($requete);
      return $query->row();
    }
    public function get_all_commandes() {
      $requete = "SELECT * FROM t_commande_com
                  JOIN t_point_retrait_pt USING(PT_id)
                  ORDER BY PT_Adresse;";
      $query = $this->db->query($requete);
      return $query->result();
    }
}
?>