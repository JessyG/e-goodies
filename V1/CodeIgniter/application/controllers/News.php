<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class News extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url');
	}
	public function index() {
		//$this->load->view('page_redirection');
		$this->load->view('templates/haut');
		$data['actu'] = $this->db_model->getAllNews();
		$this->load->view('page_accueil', $data);
		$this->load->view('templates/bas');
	}
}
?>