<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Connexion extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url');
	}
	function index() {
      $data['erreur'] = "";
	    $this->load->view('page_redirection');
      $this->load->helper('form');
      $this->load->library('form_validation');
      $this->form_validation->set_rules('pseudo', 'pseudo', 'required', array('required' => 'Pseudo non saisi.'));
      $this->form_validation->set_rules('mdp', 'mdp', 'required', array('required' => 'Mot de passe non saisi.'));
      if ($this->form_validation->run() == FALSE) { 
        $this->load->view('templates/haut');
        $this->load->view('page_connexion', $data);
        $this->load->view('templates/bas');
      }
      else {
        $login = $this->input->post('pseudo');
        $password = $this->input->post('mdp');
        if($this->db_model->connect_compte($login,$password)) {
          $statut = $this->db_model->get_statut($login);
          $session_data = array('login' => $login,
          						          'statut' => $statut);
          $this->session->set_userdata($session_data);
          $this->load->view('templates/haut');
          $this->load->view('page_connexion_action');
          $this->load->view('templates/bas');
        }
        else {
          $data['erreur'] = "Echec d’authentification, veuillez réessayez.";
          $this->load->view('templates/haut');
          $this->load->view('page_connexion', $data);
          $this->load->view('templates/bas');
        }
      }
  }
    
	public function deconnexion() {
		//$this->load->view('page_redirection_visiteur');
		$this->load->view('page_deconnexion');
	}
}
?>