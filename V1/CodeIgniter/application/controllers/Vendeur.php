<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vendeur extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url');
	}
	public function index() {
		$this->load->view('page_redirection_vendeur');
		$this->load->view('templates/hautVendeur');
		$data['actu'] = $this->db_model->getAllNews();
		$this->load->view('page_accueil', $data);
		$this->load->view('templates/bas');
	}
	public function profil() {
		$this->load->view('page_redirection_vendeur');
		$data['erreur'] = "";
		$data['row'] = $this->db_model->get_profil();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('prenom', 'prenom', 'required', array('required' => 'prenom non saisi.'));
		$this->form_validation->set_rules('nom', 'nom', 'required', array('required' => 'nom non saisi.'));
		$this->form_validation->set_rules('mail', 'mail', 'required', array('required' => 'email non saisie.'));
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/hautVendeur');
			$this->load->view('page_profil_vendeur', $data);
			$this->load->view('templates/bas');
		}
		else {
			$prenom = addslashes($this->input->post('prenom'));
			$nom = addslashes($this->input->post('nom'));
			$mail = addslashes($this->input->post('mail'));
			$mdp1 = addslashes($this->input->post('mdp1'));
			$mdp2 = addslashes($this->input->post('mdp2'));
			if($this->db_model->check_profil($prenom, $nom, $mail, $mdp1)) {
				$this->load->view('templates/hautVendeur');
				$this->load->view('page_profil_vendeur', $data);
				$this->load->view('templates/bas');
			}
			else if($this->db_model->check_mdp($mdp1, $mdp2) == FALSE) {
				$data['erreur'] = "Confirmation du mot de passe erronée, Veuillez réessayer";
				$this->load->view('templates/hautVendeur');
				$this->load->view('page_profil_vendeur', $data);
				$this->load->view('templates/bas');
			}
			else {
				if(!empty($mdp1)) {
					$this->db_model->update_password($mdp1);
				}
				$this->db_model->update_profil($prenom, $nom, $mail);
				$data['erreur'] = "Mise à jour effectuée. Vous pouvez recharger la page pour afficher les modifications.";
				$this->load->view('templates/hautVendeur');
				$this->load->view('page_profil_vendeur', $data);
				$this->load->view('templates/bas');
			}
		}
	}
}
?>