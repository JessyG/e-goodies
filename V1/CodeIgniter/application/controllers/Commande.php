<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Commande extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url');
	}
	
	public function formulaire() {
		$data['erreur'] = "";
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('codeClient', 'codeClient', 'required', array('required' => 'Code client non saisi.'));
		$this->form_validation->set_rules('codeCommande', 'codeCommande', 'required',  array('required' => 'Code commande non saisi.'));
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/haut');
			$this->load->view('page_commande_formulaire', $data);
			$this->load->view('templates/bas');
		}
		else {
			$codeCom = $this->input->post('codeCommande');
			$codeCli = $this->input->post('codeClient');
			if($this->db_model->check_commande($codeCli,$codeCom)) {
				$data['row'] = $this->db_model->get_commande("$codeCli", "$codeCom");
				$data['goodie'] = $this->db_model->get_goodies_from_commande("$codeCli", "$codeCom");
				$data['prixTotal'] = $this->db_model->get_prix_total("$codeCli", "$codeCom");
				$this->load->view('templates/haut');
				$this->load->view('page_recapitulatif', $data);
				$this->load->view('templates/bas');
			}
			else {
				$data['erreur'] = "Code(s) erroné(s).";
				$this->load->view('templates/haut');
				$this->load->view('page_commande_formulaire', $data);
				$this->load->view('templates/bas');
			}
		}
	}
}
?>