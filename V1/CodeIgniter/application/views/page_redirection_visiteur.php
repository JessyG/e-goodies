<?php
/**
	Redirection lorqu'un visiteur/client tente d'accéder à une page vendeur ou gestionnaire
**/
//session_start();
if(!isset($_SESSION['login']))
{
  // Si la session n'est pas ouverte, redirection vers la page du formulaire
  redirect(base_url()."index.php/connexion");
  exit();
}
?>