<?php
if(isset($row)) {
echo '<article class="col-xs-12 maincontent">
	<header class="page-header">
		<h1 class="page-title">Informations sur votre profil</h1>
	</header>
			
	<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
		<div class="panel panel-default">
			<div class="panel-body">
				<hr>';
echo validation_errors();
echo form_open('vendeur/profil');
				echo '<p style="color: red">'.$erreur.'</p>
				<form method="post">
					<div class="top-margin">
						<label>Pseudo</label>
						<input type="input" name="pseudo" placeholder="pseudo" disabled="disabled" value="'.$row->CPT_Pseudo.'" maxlength=30 class="form-control">
					</div>
					<div class="top-margin">
						<label>Prenom<span class="text-danger">*</span></label>
						<input type="input" name="prenom" placeholder="prenom" pattern="[a-zA-Z]+" value="'.$row->PFL_Prenom.'" maxlength=30 class="form-control">
					</div>
					<div class="top-margin">
						<label>Nom<span class="text-danger">*</span></label>
						<input type="input" name="nom" placeholder="nom" pattern="[a-zA-Z]+" value="'.$row->PFL_Nom.'" maxlength=30 class="form-control">
					</div>
					<div class="top-margin">
						<label>Mail<span class="text-danger">*</span></label>
						<input type="input" name="mail" placeholder="mail" pattern="[a-zA-Z0-9]+@[a-zA-Z]+\.[a-zA-Z]+" value="'.$row->PFL_Mail.'" maxlength=70 class="form-control">
					</div>
					<div class="top-margin">
						<label>Point de retrait<span class="text-danger"></span></label>
						<input type="input" name="pointRetrait" placeholder="pointRetrait" value="'.$row->PT_Adresse.'" disabled="disabled" class="form-control">
					</div>
					<div class="top-margin">
						<label>Nouveau mot de passe<span class="text-danger"></span></label>
						<input type="password" name="mdp1" placeholder="nouveau mot de passe" pattern="[a-zA-Z0-9]+" minlength=8 maxlength=30 class="form-control">
					</div>
					<div class="top-margin">
						<label>Confirmation du mot de passe<span class="text-danger"></span></label>
						<input type="password" name="mdp2" placeholder="confirmation du mot de passe" pattern="[a-zA-Z0-9]+" minlength=8 maxlength=30 class="form-control">
					</div>
				<hr>
					<div class="row">
						<div class="col-lg-4 text-right">
							<button class="btn btn-action" type="submit">Valider</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>				
</article>';
}