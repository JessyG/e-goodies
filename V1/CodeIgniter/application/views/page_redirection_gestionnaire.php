<?php
/**
	Redirection lorqu'un visiteur/client ou vendeur tente d'accéder à une page gestionnaire
**/
//session_start();
if(!isset($_SESSION['login']))
{
  // Si la session n'est pas ouverte, redirection vers la page du formulaire
  redirect(base_url()."index.php/connexion");
  exit();
}
if($_SESSION['statut'] == 'V')
{
  // Si la session est ouverte, mais que le compte connecté est un vendeur
  // Redirection vers la page vendeur_accueil
  redirect(base_url()."index.php/vendeur");
  exit();
}
?>