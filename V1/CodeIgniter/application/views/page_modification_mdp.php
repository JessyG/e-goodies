<ol class="breadcrumb">
	<?php
		echo "<li><a href=";
		echo base_url();
		echo ">Home</a></li>";
	?>
	<li class="active">User access</li>
</ol>

<div class="row">
			
<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Modification du mot de passe</h1>
		</header>
				
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="thin text-center">Modifier son mot de passe</h3>
					<hr>
					<form action="<?php echo base_url();?>index.php/connexion/validationPassword" method="post">
						<div class="top-margin">
							<label>Nouveau mot de passe <span class="text-danger">*</span></label>
							<input type="password" name="passwordNew" placeholder="mot de passe" minlength="8" maxlength="30" required="required" class="form-control">
						</div>
						<div class="top-margin">
							<label>Confirmation du mot de passe <span class="text-danger">*</span></label>
							<input type="password" name="passWordConf" placeholder="mot de passe" minlength="8" maxlength="30" required="required" class="form-control">
						</div>
					<hr>
						<div class="row">
							<div class="col-lg-4 text-right">
								<button class="btn btn-action" type="submit">Valider</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>				
	</article>
</div>