<?php
//session_start();
if(isset($_SESSION['login']))
{
  // Si la session est ouverte
  if($_SESSION['statut'] == 'V')
  {
     // Si le compte connecté est un vendeur
     // Redirection vers la page vendeur_accueil
     redirect("vendeur");
     exit();
  }
  else if($_SESSION['statut'] == 'A')
  {
     // Si le compte connecté est un gestionnaire
     // Redirection vers la page gestionnaire_accueil
     redirect("gestionnaire");
     exit();
  }
}
?>