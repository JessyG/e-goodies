	</div>

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p><br>
								<a href="mailto:#">Jessy.Grall@etudiant.univ-brest.fr</a><br>
								<br>
								29200 Brest
							</p>	
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href='<?php echo base_url();?>'>Home</a> | 
								<a >Galerie</a> |
								<a href="<?php echo base_url();?>index.php/commande/formulaire">Suivi de commande</a> |
								<a>Mon panier</a> |
								<b><a href='<?php echo base_url();?>index.php/connexion'>Connexion</a></b>
							</p>
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>

	</footer>	

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src='<?php echo base_url();?>style/js/headroom.min.js'></script>
	<script src='<?php echo base_url();?>style/js/jQuery.headroom.min.js'></script>
	<script src='<?php echo base_url();?>style/js/template.js'></script>
</body>
</html>