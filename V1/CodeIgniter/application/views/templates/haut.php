<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Accueil</title>
	<link rel="shortcut icon" href='<?php echo base_url();?>style/images/icones/gt_favicon.png'>
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href='<?php echo base_url();?>style/css/bootstrap.min.css'>
	<link rel="stylesheet" href='<?php echo base_url();?>style/css/font-awesome.min.css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url();?>style/css/bootstrap-theme.css' media='screen' />
	<link rel="stylesheet" href='<?php echo base_url();?>style/css/main.css'>
</head>
<body class="home">
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href='<?php echo base_url();?>'><img src="<?php echo base_url();?>style/images/icones/logo.png" alt="logo"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href='<?php echo base_url();?>'>Home</a></li>
					<li><a>Galerie</a></li>
					<li><a href="<?php echo base_url();?>index.php/commande/formulaire">Suivi de commande</a></li>
					<li><a>Mon panier</a></li>
					<li><a class="btn" href="<?php echo base_url();?>index.php/connexion/">Connexion</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>
	<div class="container">
