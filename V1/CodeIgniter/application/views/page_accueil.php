<header class="page-header">
	<h1 class="page-title">Liste des actualités</h1>
</header>
<?php
echo "<table class='table table-condensed'>
      <thead>
       <tr>
        <th>ID</th>
        <th>TITRE</th>
        <th>CONTENU</th>
        <th>DATE</th>
        <th>AUTEUR</th>
      </tr>
      </thead>
      <tbody>";
foreach ($actu as $row) {
	echo "<tr>";
     echo ("<td>".$row->ACT_id."</td>");
     echo ("<td>".$row->ACT_Titre."</td>");
     echo ("<td>".$row->ACT_Contenu."</td>");
     echo ("<td>".$row->ACT_Date."</td>");
     echo ("<td>".$row->CPT_Pseudo."</td>");
	echo "</tr>";
}
echo "</tbody>";
echo "</table>";
?>
