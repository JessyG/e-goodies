<?php 
//Ouverture d'une session
session_start();

// libération des variables globales associées à la session
unset($_SESSION['login']);
// libération des variables globales associées à la session
unset($_SESSION['statut']);

// destruction de la session
session_destroy();
redirect(base_url());
?>