<header class="page-header">
	<h1 class="page-title">Liste des goodies dans votre panier</h1>
</header>

<h1 style="color: green;"><?php echo $erreur; ?></h1>

<?php echo form_open('panier'); 
//var_dump($this->cart->get_item(1))?>

<table class='table table-condensed'>
<thead>
<tr>
	<th>Nom</th>
	<th>Prix</th>
	<th>Quantite</th>
</tr>
</thead>
<tbody>
<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

        <tr>
                <td>
                        <?php echo $items['name']; ?>

                        <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                <p>
                                        <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                                                <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

                                        <?php endforeach; ?>
                                </p>

                        <?php endif; ?>

                </td>
                <td><?php echo $this->cart->format_number($items['price']); ?>€</td>
                <td><?php echo $this->cart->format_number($items['qty']); ?></td>
        </tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
        <td colspan="2"> </td>
        <td class="right"><strong>Total</strong></td>
        <td class="right"><?php echo $this->cart->format_number($this->cart->total()); ?>€</td>
</tr>
</tbody>
</table>

<a href="<?php echo base_url();?>index.php/panier/formulaire/" <button class="btn btn-action" type="submit">Valider mon panier</button></a>
<a href="<?php echo base_url();?>index.php/panier/vider/" <button class="btn btn-action" style="background-color: red;" type="submit">Vider mon panier</button></a>
