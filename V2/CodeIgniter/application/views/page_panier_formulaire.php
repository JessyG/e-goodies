
<div class="row">
			
<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Validation du panier</h1>
		</header>
				
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="thin text-center">Insérez vos informations personnelles</h3>
					<hr>
					<?php echo validation_errors(); ?>
					<?php echo form_open('panier/valider'); ?>
					<p style="color: red"><?php echo($erreur); ?></p>
					<form method="post">
						<div class="top-margin">
							<label>Point de retrait : </label>
							<SELECT name="ptretrait" size="1">
							<?php
							foreach($pointRetrait as $row) {
								echo '<OPTION>'.$row->PT_Adresse;
							}
							?>
							</SELECT>
						</div>
						<div class="top-margin">
							<label>Prenom <span class="text-danger">*</span></label>
							<input type="text" name="prenom" placeholder="prenom" pattern="[a-zA-Z]+" value="<?php echo set_value('prenom')?>" maxlength=30 class="form-control">
						</div>
						<div class="top-margin">
							<label>Nom <span class="text-danger">*</span></label>
							<input type="text" name="nom" placeholder="nom" pattern="[a-zA-Z]+" value="<?php echo set_value('nom')?>" maxlength=30 class="form-control">
						</div>
						<div class="top-margin">
							<label>Mail <span class="text-danger">*</span></label>
							<input type="text" name="mail" placeholder="mail" pattern="[a-zA-Z0-9]+@[a-zA-Z]+\.[a-zA-Z]+" value="<?php echo set_value('mail')?>" maxlength=70 class="form-control">
						</div>
					<hr>
						<div class="row">
							<div class="col-lg-4 text-right">
								<button class="btn btn-action" type="submit">Valider</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>				
	</article>
</div>