<header class="page-header">
	<h1 class="page-title">Liste de vos commandes</h1>
</header>
<?php
echo "<table class='table table-condensed'>
      <thead>
       <tr>
        <th>ID</th>
        <th>NOM</th>
        <th>PRENOM</th>
        <th>MAIL</th>
        <th>PRIX</th>
        <th>DATE COMMANDE</th>
        <th>DATE RETRAIT</th>
        <th>ETAT</th>
      </tr>
      </thead>
      <tbody>";
foreach ($commandes as $commande) {
	echo "<tr>";
     echo ("<td>".$commande->COM_id."</td>");
     echo ("<td>".$commande->COM_NomAcheteur."</td>");
     echo ("<td>".$commande->COM_PrenomAcheteur."</td>");
     echo ("<td>".$commande->COM_MailAcheteur."</td>");
     echo ("<td>".$commande->COM_Prix."</td>");
     echo ("<td>".$commande->COM_DateCommande."</td>");
     echo ("<td>".$commande->COM_DateRetrait."</td>");
     echo ("<td>".$commande->COM_Etat."</td>");
	echo "</tr>";
}
echo "</tbody>";
echo "</table>";
?>
<?php echo validation_errors(); ?>
<?php echo form_open('vendeur/commande'); ?>
<label>Sélectionner l'id de la commande : </label>
<SELECT name="idCOM" size="1">
<?php
foreach($id as $row) {
  echo '<OPTION>'.$row->COM_id;
}
?>
</SELECT>
<label>Sélectionner l'état de la commande : </label>
<SELECT name="etatCOM" size="1">
  <OPTION>Annulée</OPTION>
  <OPTION>Retirée</OPTION>
</SELECT>
<button class="btn btn-action" type="submit">Valider</button>

