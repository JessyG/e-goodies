<?php
/**
	Redirection lorqu'un visiteur/client ou gestionnaire tente d'accéder à une page vendeur
**/
session_start();
if(!isset($_SESSION['login']))
{
  // Si la session n'est pas ouverte, redirection vers la page du formulaire
  redirect(base_url()."index.php/connexion");
  exit();
}
if($_SESSION['statut'] == 'A')
{
  // Si la session est ouverte, mais que le compte connecté est un gestionnaire
  // Redirection vers la page d'accueil du gestionnaire
  redirect(base_url()."index.php/gestionnaire");
  exit();
}
?>