
<div class="row">
			
<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Création d'un compte</h1>
		</header>
				
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="thin text-center">Insérez les données du compte à créer</h3>
					<hr>
					<?php echo validation_errors(); ?>
					<?php echo form_open('gestionnaire/ajouter_compte'); ?>
					<p style="color: red"><?php echo($erreur); ?></p>
					<form method="post">
						<div class="top-margin">
							<label>Choix du statut : </label>
							<input type="radio" name="statut" value="gestionnaire" checked="checked"> gestionnaire
							<input type="radio" name="statut" value="vendeur"> vendeur<br>
						</div>
						<div class="top-margin">
							<label>Point de retrait : </label>
							<SELECT name="ptretrait" size="1">
							<?php
							foreach($pointRetrait as $row) {
								echo '<OPTION>'.$row->PT_Adresse;
							}
							?>
							</SELECT>
						</div>
						<div class="top-margin">
							<label>Pseudo <span class="text-danger">*</span></label>
							<input type="text" name="pseudo" placeholder="pseudo" pattern="[a-zA-Z0-9]+" value="<?php echo set_value('pseudo')?>" maxlength=20 class="form-control">
						</div>
						<div class="top-margin">
							<label>Prenom <span class="text-danger">*</span></label>
							<input type="text" name="prenom" placeholder="prenom" pattern="[a-zA-Z]+" value="<?php echo set_value('prenom')?>" maxlength=30 class="form-control">
						</div>
						<div class="top-margin">
							<label>Nom <span class="text-danger">*</span></label>
							<input type="text" name="nom" placeholder="nom" pattern="[a-zA-Z]+" value="<?php echo set_value('nom')?>" maxlength=30 class="form-control">
						</div>
						<div class="top-margin">
							<label>Mail <span class="text-danger">*</span></label>
							<input type="text" name="mail" placeholder="mail" pattern="[a-zA-Z0-9]+@[a-zA-Z]+\.[a-zA-Z]+" value="<?php echo set_value('mail')?>" maxlength=70 class="form-control">
						</div>
						<div class="top-margin">
							<label>Mot de passe <span class="text-danger">*</span></label>
							<input type="password" name="mdp1" placeholder="mot de passe" pattern="[a-zA-Z0-9]+" minlength=8 maxlength=30 class="form-control">
						</div>
						<div class="top-margin">
							<label>Confirmation du mot de passe <span class="text-danger">*</span></label>
							<input type="password" name="mdp2" placeholder="confirmation du mot de passe" pattern="[a-zA-Z0-9]+" minlength=8 maxlength=30 class="form-control">
						</div>
					<hr>
						<div class="row">
							<div class="col-lg-4 text-right">
								<button class="btn btn-action" type="submit">Valider</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>				
	</article>
</div>