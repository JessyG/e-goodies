<header class="page-header">
	<h1 class="page-title">Liste des commandes</h1>
</header>
<?php
echo "<table class='table table-condensed'>
      <thead>
       <tr>
        <th>PT RETRAIT</th>
        <th>ID COMMANDE</th>
        <th>PRENOM</th>
        <th>NOM</th>
        <th>MAIL</th>
        <th>PRIX</th>
        <th>DATE COMMANDE</th>
        <th>DATE RETRAIT</th>
        <th>ETAT</th>
      </tr>
      </thead>
      <tbody>";
foreach ($commande as $row) {
	echo "<tr>";
     echo ("<td>".$row->PT_Adresse."</td>");
     echo ("<td>".$row->COM_id."</td>");
     echo ("<td>".$row->COM_PrenomAcheteur."</td>");
     echo ("<td>".$row->COM_NomAcheteur."</td>");
     echo ("<td>".$row->COM_MailAcheteur."</td>");
     echo ("<td>".$row->COM_Prix."€</td>");
     echo ("<td>".$row->COM_DateCommande."</td>");
     echo ("<td>".$row->COM_DateRetrait."</td>");
     echo ("<td>".$row->COM_Etat."</td>");
	echo "</tr>";
}
echo "</tbody>";
echo "</table>";
?>
