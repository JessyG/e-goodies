<header class="page-header">
	<h1 class="page-title">Liste des originaux</h1>
</header>
<?php echo validation_errors(); ?>
<?php echo form_open('galerie'); ?>
<form method="post">
	<label>Choisissez une catégorie : </label>
	<SELECT name="categorie" size="1">
	<?php
	foreach($categories as $categorie) {
		echo '<OPTION>'.$categorie->CAT_Nom;
	}
	?>
	</SELECT>
	<button class="btn btn-action" type="submit">Go</button>
</form>
<?php
$test = "";
foreach($originaux as $original) {
	if($test != $original->CAT_Nom) {
		echo "<h1>".$original->CAT_Nom."</h1>";
		$test = $original->CAT_Nom;
	}
	echo '<div class="blocImage">';
            echo'<h4><strong>'.$original->ORI_Nom.'</strong></h4>';
            echo "<a href=".base_url()."index.php/galerie/original/".$original->ORI_id." </a> <img class='displayed' src='".base_url()."style/images/originaux/".$original->ORI_Image."'></a>";
    echo '</div>';
}
?>