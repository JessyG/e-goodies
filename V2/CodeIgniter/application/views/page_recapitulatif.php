<header class="page-header">
    <h1 class="page-title">Récapitulatif de la commande</h1>
</header>
<div class="recapitulatif">
    <?php
    if(isset($row)) {
        echo '
        <strong>
            <div style="float:left;">
                <p>Nom : '.$row->COM_NomAcheteur.'</p>
                <p>Prénom : '.$row->COM_PrenomAcheteur.'</p>
                <p>Adresse mail : '.$row->COM_MailAcheteur.'</p>
            </div> 
            <div style="float:right;">
                <p>Code client : '.$row->COM_CodeClient.'</p>
                <p>Code commande : '.$row->COM_CodeCommande.'</p>
                <p>Date de la commande : '.$row->COM_DateCommande.'</p>
                <p>Date retrait : '.$row->COM_DateRetrait.'</p>
                <p>État de la commande : '.$row->COM_Etat.'</p>';
            if(isset($prixTotal)) {
                echo'<p><strong>Prix total : </strong>'.$prixTotal->prixMax.'€</p>';
            }
            echo'
             </div>
        </strong>';
    }
    ?>
    </br>
    </br>
    </br>
    </br>
    </br>
    </br>
    
    <?php
    echo "<div class='box'>";
    foreach($goodie as $row) {
        echo '<div class="blocImage">';
            echo'<h4><strong>'.$row->GOO_Nom.'</strong></h4>';
            echo ("<img class='displayed' src='".base_url()."style/images/goodies/".$row->GOO_Image."'>");
            echo ("<p>".$row->GOO_Description."</p>");
            echo ("<p><strong> Quantite commandée : </strong>".$row->GOOCOM_QuantiteCommande."</p>");
            echo ("<p><strong> Prix unitaire : </strong>".$row->GOO_PrixImage."€</p>");
        echo '</div>';
    }
    echo "</div>";
    ?>
</div>
</br>
</br>
<?php echo form_open('commande/supprimer');
if(isset($row)) :?>
    <input type="hidden" name="id" value='<?php echo $row->COM_id;?>'>
    <button class="btn btn-action" style="background-color: red;" type="submit">Supprimer ma commande</button>
<?php endif; ?>
