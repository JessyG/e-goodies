<div class="row">
			
<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Suivi de mes commandes</h1>
		</header>
				
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="thin text-center">Insérez vos codes</h3>
					<hr>
					<?php echo validation_errors(); ?>
					<?php echo form_open('commande/formulaire'); ?>
					<p style="color: red"><?php echo($erreur); ?></p>
					<form method="post">
						<div class="top-margin">
							<label>Code client <span class="text-danger">*</span></label>
							<input type="input" name="codeClient" placeholder="code client" minlength="8" maxlength="8" class="form-control">
						</div>
						<div class="top-margin">
							<label>Code commande <span class="text-danger">*</span></label>
							<input type="input" name="codeCommande" placeholder="code commande" minlength="20" maxlength="20" class="form-control">
						</div>
					<hr>
						<div class="row">
							<div class="col-lg-4 text-right">
								<button class="btn btn-action" type="submit">Valider</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>				
	</article>
</div>