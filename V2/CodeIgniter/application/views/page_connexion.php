<ol class="breadcrumb">
	<li><a href="index.php">Home</a></li>
	<li class="active">User access</li>
</ol>

<div class="row">
			
<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Connexion</h1>
		</header>
				
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3 class="thin text-center">Connexion à ton compte</h3>
					<hr>
					<?php echo validation_errors(); ?>
					<?php echo form_open('connexion'); ?>
					<p style="color: red"><?php echo($erreur); ?></p>
					<form method="post">
						<div class="top-margin">
							<label>Pseudo <span class="text-danger">*</span></label>
							<input type="text" name="pseudo" placeholder="pseudo" maxlength=20 class="form-control">
						</div>
						<div class="top-margin">
							<label>Mot de passe <span class="text-danger">*</span></label>
							<input type="password" name="mdp" placeholder="mot de passe" maxlength=30 class="form-control">
						</div>
					<hr>
						<div class="row">
							<div class="col-lg-4 text-right">
								<button class="btn btn-action" type="submit">Valider</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>				
	</article>
</div>