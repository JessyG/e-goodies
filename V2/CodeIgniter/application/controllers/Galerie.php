<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Galerie extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->library('cart');
		$this->load->helper('url');
	}
	
	public function index() {
		$this->load->view('page_redirection');
		$data['categories'] = $this->db_model->get_all_categories();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('categorie', 'categorie', 'required', array('required' => 'Veuillez choisir une catégorie'));
		if ($this->form_validation->run() == FALSE) {
			$data['originaux'] = $this->db_model->get_all_originaux();
			$this->load->view('templates/haut');
			$this->load->view('page_galerie', $data);
			$this->load->view('templates/bas');
		}
		else {
			$categorie = $this->input->post('categorie');
			$data['originaux'] = $this->db_model->get_originaux_by_cat($categorie);
			$this->load->view('templates/haut');
			$this->load->view('page_galerie', $data);
			$this->load->view('templates/bas');
		}
	}

	public function original($id) {
		$this->load->view('page_redirection');
		$data['original'] = $this->db_model->get_original($id);
		$data['goodies'] = $this->db_model->get_goodies_from_original($id);
		$this->load->view('templates/haut');
		$this->load->view('page_original', $data);
		$this->load->view('templates/bas');
	}
}
?>