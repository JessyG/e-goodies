<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Goodie extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->library('cart');
		$this->load->helper('url');
	}

	public function index() {
		$this->load->view('page_redirection');
		$data['types'] = $this->db_model->get_all_types();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('type', 'type', 'required', array('required' => 'Veuillez choisir une catégorie'));
		if ($this->form_validation->run() == FALSE) {
			$data['goodies'] = $this->db_model->get_all_goodies();
			$this->load->view('templates/haut');
			$this->load->view('page_goodies', $data);
			$this->load->view('templates/bas');
		}
		else {
			$type = $this->input->post('type');
			$data['goodies'] = $this->db_model->get_goodies_from_typ($type);
			$this->load->view('templates/haut');
			$this->load->view('page_goodies', $data);
			$this->load->view('templates/bas');
		}
	}

	public function numero($id) {
		$this->load->view('page_redirection');
		$data['erreur'] = "";
		$data['goodie'] = $this->db_model->get_goodie($id);
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('quantite', 'quantite', 'required', array('required' => 'Veuillez choisir une quantité'));
		if ($this->form_validation->run() == TRUE) {
			$quantite = $this->input->post('quantite');
			$goo_id = $this->input->post('goo_id');
			$infoGoo = $this->db_model->get_goodie($goo_id);
			if(isset($infoGoo)) {
				if($quantite > $infoGoo->GOO_Quantite) {
					$data['erreur'] = "Quantité indisponible";
				}
				else {
					$dataC = array(
        					'id'    => $goo_id,
        					'qty'   => $quantite,
        					'price' => $infoGoo->GOO_PrixImage,
        					'name'  => $infoGoo->GOO_Nom
							);
					$this->cart->insert($dataC);
					$data['erreur'] = "Goodie ajouté au panier avec succès";
				}
			}
			$this->load->view('templates/haut');
			$this->load->view('page_goodie', $data);
			$this->load->view('templates/bas');
		}
		else {
			$this->load->view('templates/haut');
			$this->load->view('page_goodie', $data);
			$this->load->view('templates/bas');
		}
	}
}
?>