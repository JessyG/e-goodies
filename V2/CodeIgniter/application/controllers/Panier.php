<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Panier extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->library('cart');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	public function index() {
		$data['erreur'] = "";
		$this->load->view('page_redirection');
		$this->load->view('templates/haut');  
        $this->load->view('page_panier', $data);
        $this->load->view('templates/bas');
	}

	public function vider() {
		$this->load->view('page_redirection');
		$this->cart->destroy();
		redirect('panier');
	}

	public function formulaire() {
		$this->load->view('page_redirection');
		$test = 0;
		if($this->cart->total_items() > 0) {
			foreach($this->cart->contents() as $items) {
				if($items['qty'] > $this->db_model->get_quantite_goodie($items['id'])) {
					$test = 1;
					$data['erreur'] = "Stock insuffisant";
					$this->load->view('templates/haut');
					$this->load->view('page_panier', $data);
					$this->load->view('templates/bas');
					break;
				}
			}
			if($test == 0) {
				$data['erreur'] = "";
				$data['pointRetrait'] = $this->db_model->get_all_ptretrait();
				$this->load->view('templates/haut');
        		$this->load->view('page_panier_formulaire', $data);
        		$this->load->view('templates/bas');
			}
		}
		else {
			$data['erreur'] = "Vous n'avez aucun goodie dans votre panier";
			$this->load->view('templates/haut');
			$this->load->view('page_panier', $data);
			$this->load->view('templates/bas');
		}
	}

	public function valider() {
		$this->load->view('page_redirection');
		$data['erreur'] = "";
		$data['pointRetrait'] = $this->db_model->get_all_ptretrait();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('prenom', 'prenom', 'required', array('required' => 'prenom non saisi'));
		$this->form_validation->set_rules('nom', 'nom', 'required', array('required' => 'nom non saisi'));
		$this->form_validation->set_rules('mail', 'mail', 'required', array('required' => 'email non saisie'));
		if ($this->form_validation->run() == FALSE) {
			if($this->cart->total_items() > 0) {
				$this->load->view('templates/haut');
				$this->load->view('page_panier_formulaire', $data);
				$this->load->view('templates/bas');
			}
			else {
				$data['erreur'] = "Vous n'avez aucun goodie dans votre panier";
				$this->load->view('templates/haut');
				$this->load->view('page_panier', $data);
				$this->load->view('templates/bas');
			}
		}
		else {
			if($this->cart->total_items() > 0) {
				$prenom = addslashes($this->input->post('prenom'));
				$nom = addslashes($this->input->post('nom'));
				$mail = addslashes($this->input->post('mail'));
				$ptretrait = $this->input->post('ptretrait');
				$this->db_model->create_commande($prenom, $nom, $mail, $ptretrait);
				$idMax = $this->db_model->get_max_id_commande();
				$codes = $this->db_model->get_codes_from_id($idMax);
				if(isset($codes)) {
					$data['row'] = $this->db_model->get_commande($codes->COM_CodeClient, $codes->COM_CodeCommande);
					$data['goodie'] = $this->db_model->get_goodies_from_commande($codes->COM_CodeClient, $codes->COM_CodeCommande);
					$data['prixTotal'] = $this->db_model->get_prix_total($codes->COM_CodeClient, $codes->COM_CodeCommande);
					$this->load->view('templates/haut');
					$this->load->view('page_recapitulatif', $data);
					$this->load->view('templates/bas');
					$this->cart->destroy();
				}
			}
			else {
				$data['erreur'] = "Vous n'avez aucun goodie dans votre panier";
				$this->load->view('templates/haut');
				$this->load->view('page_panier', $data);
				$this->load->view('templates/bas');
			}
		}
	}
}
?>