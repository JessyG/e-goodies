<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gestionnaire extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('db_model');
		$this->load->helper('url');
	}
	public function index() {
		$this->load->view('page_redirection_gestionnaire');
		$this->load->view('templates/hautGestionnaire');
		$data['actu'] = $this->db_model->getAllNews();
		$this->load->view('page_accueil', $data);
		$this->load->view('templates/bas');
	}
	public function profil() {
		$this->load->view('page_redirection_gestionnaire');
		$data['erreur'] = "";
		$data['row'] = $this->db_model->get_profil();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('prenom', 'prenom', 'required', array('required' => 'prenom non saisi'));
		$this->form_validation->set_rules('nom', 'nom', 'required', array('required' => 'nom non saisi'));
		$this->form_validation->set_rules('mail', 'mail', 'required', array('required' => 'email non saisie'));
		if ($this->form_validation->run() == FALSE) {
			$data['erreur'] = $this->session->flashdata('test');
			$this->load->view('templates/hautGestionnaire');
			$this->load->view('page_profil_gestionnaire', $data);
			$this->load->view('templates/bas');
		}
		else {
			$prenom = addslashes($this->input->post('prenom'));
			$nom = addslashes($this->input->post('nom'));
			$mail = addslashes($this->input->post('mail'));
			$mdp1 = addslashes($this->input->post('mdp1'));
			$mdp2 = addslashes($this->input->post('mdp2'));
			if($this->db_model->check_profil($prenom, $nom, $mail, $mdp1)) {
				$this->load->view('templates/hautGestionnaire');
				$this->load->view('page_profil_gestionnaire', $data);
				$this->load->view('templates/bas');
			}
			else if($this->db_model->check_mdp($mdp1, $mdp2) == FALSE) {
				$data['erreur'] = "Confirmation du mot de passe erronée, Veuillez réessayer";
				$this->load->view('templates/hautGestionnaire');
				$this->load->view('page_profil_gestionnaire', $data);
				$this->load->view('templates/bas');
			}
			else {
				if(!empty($mdp1)) {
					$this->db_model->update_password($mdp1);
				}
				$this->db_model->update_profil($prenom, $nom, $mail);
				$this->session->set_flashdata('test', 'Mise à jour effectuée');
				redirect('gestionnaire/profil');
			}
		}
	}

	public function compte() {
		$this->load->view('page_redirection_gestionnaire');
		$data['profil'] = $this->db_model->get_all_profils();
		$this->load->view('templates/hautGestionnaire');
		$this->load->view('page_tous_les_profils', $data);
		$this->load->view('templates/bas');
	}

	public function ajouter_compte() {
		$this->load->view('page_redirection_gestionnaire');
		$data['erreur'] = "";
		$data['pointRetrait'] = $this->db_model->get_all_ptretrait();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$statut = $this->input->post('statut');
		$this->form_validation->set_rules('pseudo', 'pseudo', 'required', array('required' => 'pseudo non saisi'));
		$this->form_validation->set_rules('prenom', 'prenom', 'required', array('required' => 'prenom non saisi'));
		$this->form_validation->set_rules('nom', 'nom', 'required', array('required' => 'nom non saisi'));
		$this->form_validation->set_rules('mail', 'mail', 'required', array('required' => 'email non saisie'));
		$this->form_validation->set_rules('mdp1', 'mdp1', 'required', array('required' => 'mot de passe non saisi'));
		$this->form_validation->set_rules('mdp2', 'mdp2', 'required', array('required' => 'confirmation du mot de passe non saisie'));
		if($statut == 'vendeur') {
			$this->form_validation->set_rules('ptretrait', 'ptretrait', 'required', array('required' => 'un vendeur doit être relié à un point de retrait.'));
		}
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/hautGestionnaire');
			$this->load->view('page_ajouter_compte', $data);
			$this->load->view('templates/bas');
		}
		else {
			$pseudo = addslashes($this->input->post('pseudo'));
			$prenom = addslashes($this->input->post('prenom'));
			$nom = addslashes($this->input->post('nom'));
			$mail = addslashes($this->input->post('mail'));
			$ptretrait = $this->input->post('ptretrait');
			$mdp1 = addslashes($this->input->post('mdp1'));
			$mdp2 = addslashes($this->input->post('mdp2'));
			if($this->db_model->check_pseudo($pseudo) == TRUE) {
				$data['erreur'] = "ce pseudo existe déjà";
				$this->load->view('templates/hautGestionnaire');
				$this->load->view('page_ajouter_compte', $data);
				$this->load->view('templates/bas');
			}
			else if($this->db_model->check_mdp($mdp1, $mdp2) == FALSE) {
				$data['erreur'] = "Confirmation du mot de passe erronée, Veuillez réessayer";
				$this->load->view('templates/hautGestionnaire');
				$this->load->view('page_ajouter_compte', $data);
				$this->load->view('templates/bas');
			}
			else {
				$this->db_model->add_compte($statut, $pseudo, $prenom, $nom, $mail, $ptretrait, $mdp1);
				$data['erreur'] = "compte ajouté avec succès";
				$this->load->view('templates/hautGestionnaire');
				$this->load->view('page_ajouter_compte', $data);
				$this->load->view('templates/bas');
			}
		}
	}

	public function commandes() {
		$this->load->view('page_redirection_gestionnaire');
		$data['commande'] = $this->db_model->get_all_commandes();
		$this->load->view('templates/hautGestionnaire');
		$this->load->view('page_toutes_les_commandes', $data);
		$this->load->view('templates/bas');
	}
}
?>